var dsTask = [];
var dsTaskCompleted = [];
var dsTaskJSON = localStorage.getItem("DSTASK");
var dsTaskCompletedJSON = localStorage.getItem("DSTASKCOMPLETED");
let isFilter = false;
if (dsTaskJSON != null) {
  dsTask = JSON.parse(dsTaskJSON);
  // dsTaskCompleted = JSON.parse(dsTaskCompletedJSON);
  for (var index = 0; index < dsTask.length; index++) {
    var taskNew = dsTask[index];
    dsTask[index] = new TaskV(taskNew.nhiemvu);
  }
  renderTask(dsTask);
}
enterToActive("#newTask", "#addItem");
document.getElementById("addItem").addEventListener("click", function () {
  //   console.log("yes");
  let task = LayThongTinTuForm();
  let isValid = validator.kiemTraRong(
    task.nhiemvu,
    "tbTask",
    "Chưa nhập nhiệm vụ!"
  );
  if (isValid) {
    dsTask.push(task);
    let dsTaskJson = JSON.stringify(dsTask);
    localStorage.setItem("DSTASK", dsTaskJson);
    console.log("dsTask: ", dsTask);
    renderTask(dsTask);
  }
  //   console.log("task: ", task);
});
// hàm xóa việc cần làm
function xoaViecCanLam(id) {
  //   console.log("yes");
  var index = timKiemViTri(id, dsTask);
  if (index != -1) {
    dsTask.splice(index, 1);
    localStorage.setItem("DSTASK", JSON.stringify(dsTask));

    renderTask(dsTask);
  }
}
// xóa bỏ bớt việc đã làm xong
function xoaViecDaLamXong(id) {
  var index = timKiemViTri(id, dsTaskCompleted);
  if (index != -1) {
    dsTaskCompleted.splice(index, 1);
    localStorage.setItem("DSTASKCOMPLETED", JSON.stringify(dsTaskCompleted));

    showTaskCompleted(dsTaskCompleted);
  }
}
// show những việc đã làm xong
function showNhungViecDaLamXong(id) {
  var index = timKiemViTri(id, dsTask);
  if (index != -1) {
    var currentTask = dsTask[index];
    // console.log("currentTask: ", currentTask);
    dsTaskCompleted.push(currentTask);

    let dsTaskCompletedJSON = JSON.stringify(dsTaskCompleted);
    localStorage.setItem("DSTASKCOMPLETED", dsTaskCompletedJSON);
    showTaskCompleted(dsTaskCompleted);
  }
  //   console.log("yes");
}
// start descending sort
document.getElementById("three").onclick = function () {
  // sort is used to sort the elements
  dsTask.sort(function (a, b) {
    return b.nhiemvu.localeCompare(a.nhiemvu);
  });
  renderTask(dsTask);
};
// end descending sort

// start Sort up
document.getElementById("two").onclick = function () {
  // console.log("yes");
  // sort is used to sort the elements
  dsTask.sort(function (a, b) {
    return a.nhiemvu.localeCompare(b.nhiemvu);
  });
  renderTask(dsTask);
};

// end Sort up

document.querySelector("#one").addEventListener("click", () => {
  dsTaskCompleted = dsTaskCompleted.filter((task) => task.status == true);
  renderTask(dsTaskCompleted);
  isFilter = true;
});

document.querySelector("#all").addEventListener("click", () => {
  renderTask(dsTask);
  isFilter = false;
});
