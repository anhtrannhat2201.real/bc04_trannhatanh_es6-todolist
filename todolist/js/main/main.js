import {
  enterToActive,
  findMissingId,
  renderTask,
  taskControl,
} from "../controller/todo.controller.js";
import { Task } from "../model/model.js";

const LOCALSTORAGE_TODOLIST = "todoList";
let tempTaskList = localStorage.getItem(LOCALSTORAGE_TODOLIST);
let taskList = [];
let completedTaskList = [];
let filter = false;
// set task control toggle
taskControl.toggleTaskCompletion = (e) => {
  //   console.log("yes");
  let taskID = e.target.dataset.id;
  let taskIndex = taskList.findIndex((task) => task.id == taskID);
  if (taskList[taskIndex].status == false) {
    taskList[taskIndex].status = true;
  } else {
    taskList[taskIndex].status = false;
  }
  localStorage.setItem(LOCALSTORAGE_TODOLIST, JSON.stringify(taskList));

  if (filter) return renderTask(completedTaskList);
  renderTask(taskList);
};
// set remove control

taskControl.remove = (e) => {
  //   console.log("yes");
  let taskID = e.target.dataset.id;
  let taskIndex = taskList.findIndex((task) => task.id == taskID);
  taskList.splice(taskIndex, 1);
  localStorage.setItem(LOCALSTORAGE_TODOLIST, JSON.stringify(taskList));

  if (filter) {
    let completedTaskIndex = completedTaskList.findIndex(
      (task) => task.id == taskID
    );
    completedTaskList.splice(completedTaskIndex, 1);
    renderTask(completedTaskList);
    return;
  }
  renderTask(taskList);
};
// end remove
// render ra giao diện
if (tempTaskList != null) {
  taskList = JSON.parse(tempTaskList);
  taskList = taskList.map((task) => {
    return new Task(task.id, task.detail, task.status);
  });
  renderTask(taskList);
}
// add task
enterToActive("#newTask", "#addItem");
document.querySelector("#addItem").addEventListener("click", () => {
  let newTaskID = findMissingId(taskList);
  let newTaskDetail = document.querySelector("#newTask").value;
  let newTask = new Task(newTaskID, newTaskDetail, false);

  taskList.push(newTask);
  localStorage.setItem(LOCALSTORAGE_TODOLIST, JSON.stringify(taskList));
  document.querySelector("#newTask").value = "";

  if (filter) {
    completedTaskList.push(newTask);
    renderTask(completedTaskList);
    return;
  }
  renderTask(taskList);
});
// start descending sort
document.getElementById("three").onclick = function () {
  // console.log("yes");
  taskList.sort((a, b) => {
    const detailA = a.detail.toLowerCase();
    const detailB = b.detail.toLowerCase();
    if (detailA < detailB) return 1;
    if (detailA > detailB) return -1;
    return 0;
  });
  localStorage.setItem(LOCALSTORAGE_TODOLIST, JSON.stringify(taskList));

  if (filter) {
    completedTaskList.sort((a, b) => {
      const detailA = a.detail.toLowerCase();
      const detailB = b.detail.toLowerCase();
      if (detailA < detailB) return 1;
      if (detailA > detailB) return -1;
      return 0;
    });
    renderTask(completedTaskList);
    return;
  }
  renderTask(taskList);
};
// end descending sort

// start Sort up
document.getElementById("two").onclick = function () {
  // console.log("yes");
  taskList.sort((a, b) => {
    const detailA = a.detail.toLowerCase();
    const detailB = b.detail.toLowerCase();
    if (detailA < detailB) return -1;
    if (detailA > detailB) return 1;
    return 0;
  });
  localStorage.setItem(LOCALSTORAGE_TODOLIST, JSON.stringify(taskList));

  if (filter) {
    completedTaskList.sort((a, b) => {
      const detailA = a.detail.toLowerCase();
      const detailB = b.detail.toLowerCase();
      if (detailA < detailB) return -1;
      if (detailA > detailB) return 1;
      return 0;
    });
    renderTask(completedTaskList);
    return;
  }
  renderTask(taskList);
};
// end Sort up
// show những việc đã làm xong
document.querySelector("#one").addEventListener("click", () => {
  completedTaskList = taskList.filter((task) => task.status == true);
  renderTask(completedTaskList);
  filter = true;
});
// show tất cả những việc chưa làm và đã làm xong
document.querySelector("#all").addEventListener("click", () => {
  renderTask(taskList);
  filter = false;
});
