function LayThongTinTuForm() {
  const inputTask = document.getElementById("newTask").value;
  document.getElementById("newTask").value = "";
  var task = new TaskV(inputTask);
  return task;
}
// start render task
let renderTask = (arrCard) => {
  var contentHTML = "";
  arrCard.forEach((item) => {
    //   let contentTr=document.createElement("div")
    //   contentTr.classList.add("card__add")
    //   let contentInput=document.createElement("input","button")
    let contentInput = `
    <li>
    ${item.nhiemvu}
        <a class="text-warning" style="cursor:pointer" data-target="#myModal" data-toggle="modal">
            <i class="fa-regular fa-trash-can" onclick=xoaViecCanLam('${item.nhiemvu}')></i>
            <i class="fa-solid fa-circle-check" onclick=showNhungViecDaLamXong('${item.nhiemvu}')></i>

        </a>
    </li>
    `;
    contentHTML += contentInput;
  });
  document.getElementById("todo").innerHTML = contentHTML;
};
// end render task
// start timkiemVITri
function timKiemViTri(id, dsTask) {
  for (var index = 0; index < dsTask.length; index++) {
    let task = dsTask[index];
    if (task.nhiemvu == id) {
      return index;
    }
  }
  return -1;
}
// end timkiemViTri
// start showTaskCompleted
function showTaskCompleted(arrCard) {
  //   document.getElementById("newTask").value = nv.nhiemvu;
  var contentHTMLCompleted = "";
  arrCard.forEach((item) => {
    //   let contentTr=document.createElement("div")
    //   contentTr.classList.add("card__add")
    //   let contentInput=document.createElement("input","button")
    let contentInput = `
    <li>
    <span class="text-success">${item.nhiemvu}</span>
        <a class="text-warning" style="cursor:pointer" data-target="#myModal" data-toggle="modal">
            <i class="fa-regular fa-trash-can" onclick=xoaViecDaLamXong('${item.nhiemvu}')></i>
            <span class="bg-success text-white">
            <i class="fa-solid fa-circle-check" onclick=showNhungViecDaLamXong('')></i>
            </span>

        </a>
    </li>
    `;
    contentHTMLCompleted += contentInput;
  });
  document.getElementById("completed").innerHTML = contentHTMLCompleted;
}
// end showtaskCompleted
let enterToActive = (selectorInput, selectorBtn) => {
  document.querySelector(selectorInput).addEventListener("keyup", (e) => {
    if (e.key === "Enter") {
      document.querySelector(selectorBtn).click();
    }
  });
};
